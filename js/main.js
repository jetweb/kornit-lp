jQuery(document).ready(function ($) {

  $('.bullet').click(function (e) {
    $(e.currentTarget).siblings().removeClass('scale-up-tl');

    $(e.currentTarget).toggleClass('scale-up-tl');

  });

  $('.button.more-info, .button.more-info.mobile').click(function () {
    $('html,body').animate({
        scrollTop: $(".last").offset().top},
      'slow');
  });


});
